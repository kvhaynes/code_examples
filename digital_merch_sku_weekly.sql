--List of distinct SKUs in full price departments
CREATE SET VOLATILE TABLE skus AS
(
    SELECT DISTINCT sku_number
    FROM sku_lookup
    WHERE division_number NOT IN ('-1','54','90','91','92','93','94','95','96','97','98','99','120','800','900')
    /* Handles a bug if we have duplicate SKUs
        AND sku.sku_number NOT IN (
                SELECT sku_number
                FROM sku_lookup 
                WHERE div_nmbr <> 90 
                GROUP BY 1 
                HAVING COUNT(*) > 1
        )*/
) WITH DATA ON COMMIT PRESERVE ROWS;

--Location lookup table for the Direct channel
CREATE SET VOLATILE TABLE locations AS
(
    SELECT DISTINCT location
    FROM location_lookup
    WHERE channel_number = '120'
) WITH DATA ON COMMIT PRESERVE ROWS;

--Week lookup table as well as the TY month weeks should map to when they
--are used for LY values. This handles realigning for years with a 53rd week.
--To see joins where this does 53rd week realignment, find "magic"
CREATE SET VOLATILE TABLE weeks AS
(
    SELECT
        wk.fiscal_week
        , wk.fiscal_month
        , wk_next_yr.fiscal_week AS next_yr_fiscal_week
        , wk_next_yr.fiscal_month AS next_yr_fiscal_month

    FROM
        week_lookup wk
        INNER JOIN time_last_yr_by_wk_lkup_vw wk_last_yr
            ON wk.fiscal_week = wk_last_yr.last_yr_fiscal_week
        INNER JOIN week_lookup wk_next_yr
            ON wk_last_yr.fiscal_week = wk_next_yr.fiscal_week
) WITH DATA ON COMMIT PRESERVE ROWS;

--This just allows us to filter our subqueries' timeframes without
--rewriting the same logic repeatedly
CREATE MULTISET VOLATILE TABLE week_range AS
(
    --This view gives us the most recent week that has been
    --fully loaded in MADM. We decrement the year by 1 and
    --turn it into a fiscal_week with some additional math to get
    --the start week and use the fiscal_week from the view as
    --our end week
    SELECT
        ((fiscal_year - 1) * 100) + 1 AS start_fiscal_week
        , fiscal_week AS end_fiscal_week

    FROM time_last_fwk_loaded_vw
) WITH DATA ON COMMIT PRESERVE ROWS;

DELETE FROM prd_ma_digitalbi.digital_merch_sku_weekly ALL;

INSERT INTO prd_ma_digitalbi.digital_merch_sku_weekly
(
     sku_number
    ,fiscal_week
    ,fiscal_month
    ,sale_$_ty
    ,sale_units_ty
    ,sale_profit_$_ty
    ,sale_reg_$_ty
    ,sale_reg_units_ty
    ,sale_pro_$_ty
    ,sale_pro_units_ty
    ,sale_clr_$_ty
    ,sale_clr_units_ty
    ,sale_bopis_$_ty
    ,sale_bopis_units_ty
    ,sale_dtc_$_ty
    ,sale_dtc_units_ty
    ,sale_dirff_$_ty
    ,sale_dirff_units_ty
    ,sale_strff_$_ty
    ,sale_strff_units_ty
    ,rtrn_$_ty
    ,rtrn_units_ty
    ,rtrn_reg_$_ty
    ,rtrn_reg_units_ty
    ,rtrn_pro_$_ty
    ,rtrn_pro_units_ty
    ,rtrn_clr_$_ty
    ,rtrn_clr_units_ty
    ,demand_$_ty
    ,demand_units_ty
    ,demand_reg_$_ty
    ,demand_reg_units_ty
    ,demand_pro_$_ty
    ,demand_pro_units_ty
    ,demand_clr_$_ty
    ,demand_clr_units_ty
    ,demand_ds_$_ty
    ,demand_ds_units_ty
    ,demand_fls_$_ty
    ,demand_fls_units_ty
    ,demand_fc_$_ty
    ,demand_fc_units_ty
    ,shipped_$_ty
    ,shipped_units_ty
    ,shipped_reg_$_ty
    ,shipped_reg_units_ty
    ,shipped_pro_$_ty
    ,shipped_pro_units_ty
    ,shipped_clr_$_ty
    ,shipped_clr_units_ty
    ,shipped_ds_$_ty
    ,shipped_ds_units_ty
    ,shipped_fls_$_ty
    ,shipped_fls_units_ty
    ,shipped_fc_$_ty
    ,shipped_fc_units_ty
    ,receipt_$_ty
    ,receipt_units_ty
    ,receipt_po_$_ty
    ,receipt_po_units_ty
    ,receipt_po_reg_$_ty
    ,receipt_po_reg_units_ty
    ,receipt_po_clr_$_ty
    ,receipt_po_clr_units_ty
    ,receipt_rsk_$_ty
    ,receipt_rsk_units_ty
    ,receipt_rsk_reg_$_ty
    ,receipt_rsk_reg_units_ty
    ,receipt_rsk_clr_$_ty
    ,receipt_rsk_clr_units_ty
    ,receipt_dsh_$_ty
    ,receipt_dsh_units_ty
    ,receipt_dsh_reg_$_ty
    ,receipt_dsh_reg_units_ty
    ,receipt_dsh_clr_$_ty
    ,receipt_dsh_clr_units_ty
    ,inventory_$_ty
    ,inventory_units_ty
    ,inventory_regpro_$_ty
    ,inventory_regpro_units_ty
    ,inventory_clr_$_ty
    ,inventory_clr_units_ty
    ,sales$_ly
    ,sales_units_ly
    ,sale_profit_$_ly
    ,sale_reg_$_ly
    ,sale_reg_units_ly
    ,sale_pro_$_ly
    ,sale_pro_units_ly
    ,sale_clr_$_ly
    ,sale_clr_units_ly
    ,sale_bopis_$_ly
    ,sale_bopis_units_ly
    ,sale_dtc_$_ly
    ,sale_dtc_units_ly
    ,sale_dirff_$_ly
    ,sale_dirff_units_ly
    ,sale_strff_$_ly
    ,sale_strff_units_ly
    ,rtrn_$_ly
    ,rtrn_units_ly
    ,rtrn_reg_$_ly
    ,rtrn_reg_units_ly
    ,rtrn_pro_$_ly
    ,rtrn_pro_units_ly
    ,rtrn_clr_$_ly
    ,rtrn_clr_units_ly
    ,demand_$_ly
    ,demand_units_ly
    ,demand_reg_$_ly
    ,demand_reg_units_ly
    ,demand_pro_$_ly
    ,demand_pro_units_ly
    ,demand_clr_$_ly
    ,demand_clr_units_ly
    ,demand_ds_$_ly
    ,demand_ds_units_ly
    ,demand_fls_$_ly
    ,demand_fls_units_ly
    ,demand_fc_$_ly
    ,demand_fc_units_ly
    ,shipped_$_ly
    ,shipped_units_ly
    ,shipped_reg_$_ly
    ,shipped_reg_units_ly
    ,shipped_pro_$_ly
    ,shipped_pro_units_ly
    ,shipped_clr_$_ly
    ,shipped_clr_units_ly
    ,shipped_ds_$_ly
    ,shipped_ds_units_ly
    ,shipped_fls_$_ly
    ,shipped_fls_units_ly
    ,shipped_fc_$_ly
    ,shipped_fc_units_ly
    ,receipt_$_ly
    ,receipt_units_ly
    ,receipt_po_$_ly
    ,receipt_po_units_ly
    ,receipt_po_reg_$_ly
    ,receipt_po_reg_units_ly
    ,receipt_po_clr_$_ly
    ,receipt_po_clr_units_ly
    ,receipt_rsk_$_ly
    ,receipt_rsk_units_ly
    ,receipt_rsk_reg_$_ly
    ,receipt_rsk_reg_units_ly
    ,receipt_rsk_clr_$_ly
    ,receipt_rsk_clr_units_ly
    ,receipt_dsh_$_ly
    ,receipt_dsh_units_ly
    ,receipt_dsh_reg_$_ly
    ,receipt_dsh_reg_units_ly
    ,receipt_dsh_clr_$_ly
    ,receipt_dsh_clr_units_ly
    ,inventory_$_ly
    ,inventory_units_ly
    ,inventory_regpro_$_ly
    ,inventory_regpro_units_ly
    ,inventory_clr_$_ly
    ,inventory_clr_units_ly
)
SELECT
    sku_number
    , fiscal_week
    , fiscal_month
    --TY Sales
    , SUM(sales$_ty) AS sales$_ty
    , SUM(sales_units_ty) AS sales_units_ty
    , SUM(sale_profit_$_ty) AS sale_profit_$_ty
    , SUM(sale_reg_$_ty) AS sale_reg_$_ty
    , SUM(sale_reg_units_ty) AS sale_reg_units_ty
    , SUM(sale_pro_$_ty) AS sale_pro_$_ty
    , SUM(sale_pro_units_ty) AS sale_pro_units_ty
    , SUM(sale_clr_$_ty) AS sale_clr_$_ty
    , SUM(sale_clr_units_ty) AS sale_clr_units_ty
    , SUM(sale_bopis_$_ty) AS sale_bopis_$_ty
    , SUM(sale_bopis_units_ty) AS sale_bopis_units_ty
    , SUM(sale_dtc_$_ty) AS sale_dtc_$_ty
    , SUM(sale_dtc_units_ty) AS sale_dtc_units_ty
    , SUM(sale_dirff_$_ty) AS sale_dirff_$_ty
    , SUM(sale_dirff_units_ty) AS sale_dirff_units_ty
    , SUM(sale_strff_$_ty) AS sale_strff_$_ty
    , SUM(sale_strff_units_ty) AS sale_strff_units_ty
    --TY Returns
    , SUM(rtrn_$_ty) AS rtrn_$_ty
    , SUM(rtrn_units_ty) AS rtrn_units_ty
    , SUM(rtrn_reg_$_ty) AS rtrn_reg_$_ty
    , SUM(rtrn_reg_units_ty) AS rtrn_reg_units_ty
    , SUM(rtrn_pro_$_ty) AS rtrn_pro_$_ty
    , SUM(rtrn_pro_units_ty) AS rtrn_pro_units_ty
    , SUM(rtrn_clr_$_ty) AS rtrn_clr_$_ty
    , SUM(rtrn_clr_units_ty) AS rtrn_clr_units_ty
    --TY Demand
    , SUM(demand_$_ty) AS demand_$_ty
    , SUM(demand_units_ty) AS demand_units_ty
    , SUM(demand_reg_$_ty) AS demand_reg_$_ty
    , SUM(demand_reg_units_ty) AS demand_reg_units_ty
    , SUM(demand_pro_$_ty) AS demand_pro_$_ty
    , SUM(demand_pro_units_ty) AS demand_pro_units_ty
    , SUM(demand_clr_$_ty) AS demand_clr_$_ty
    , SUM(demand_clr_units_ty) AS demand_clr_units_ty
    , SUM(demand_ds_$_ty) AS demand_ds_$_ty
    , SUM(demand_ds_units_ty) AS demand_ds_units_ty
    , SUM(demand_fls_$_ty) AS demand_fls_$_ty
    , SUM(demand_fls_units_ty) AS demand_fls_units_ty
    , SUM(demand_fc_$_ty) AS demand_fc_$_ty
    , SUM(demand_fc_units_ty) AS demand_fc_units_ty
    --TY Ship
    , SUM(shipped_$_ty) AS shipped_$_ty
    , SUM(shipped_units_ty) AS shipped_units_ty
    , SUM(shipped_reg_$_ty) AS shipped_reg_$_ty
    , SUM(shipped_reg_units_ty) AS shipped_reg_units_ty
    , SUM(shipped_pro_$_ty) AS shipped_pro_$_ty
    , SUM(shipped_pro_units_ty) AS shipped_pro_units_ty
    , SUM(shipped_clr_$_ty) AS shipped_clr_$_ty
    , SUM(shipped_clr_units_ty) AS shipped_clr_units_ty
    , SUM(shipped_ds_$_ty) AS shipped_ds_$_ty
    , SUM(shipped_ds_units_ty) AS shipped_ds_units_ty
    , SUM(shipped_fls_$_ty) AS shipped_fls_$_ty
    , SUM(shipped_fls_units_ty) AS shipped_fls_units_ty
    , SUM(shipped_fc_$_ty) AS shipped_fc_$_ty
    , SUM(shipped_fc_units_ty) AS shipped_fc_units_ty
    --TY Receipts
    , SUM(receipt_$_ty) AS receipt_$_ty
    , SUM(receipt_units_ty) AS receipt_units_ty
    , SUM(receipt_po_$_ty) AS receipt_po_$_ty
    , SUM(receipt_po_units_ty) AS receipt_po_units_ty
    , SUM(receipt_po_reg_$_ty) AS receipt_po_reg_$_ty
    , SUM(receipt_po_reg_units_ty) AS receipt_po_reg_units_ty
    , SUM(receipt_po_clr_$_ty) AS receipt_po_clr_$_ty
    , SUM(receipt_po_clr_units_ty) AS receipt_po_clr_units_ty
    , SUM(receipt_rsk_$_ty) AS receipt_rsk_$_ty
    , SUM(receipt_rsk_units_ty) AS receipt_rsk_units_ty
    , SUM(receipt_rsk_reg_$_ty) AS receipt_rsk_reg_$_ty
    , SUM(receipt_rsk_reg_units_ty) AS receipt_rsk_reg_units_ty
    , SUM(receipt_rsk_clr_$_ty) AS receipt_rsk_clr_$_ty
    , SUM(receipt_rsk_clr_units_ty) AS receipt_rsk_clr_units_ty
    , SUM(receipt_dsh_$_ty) AS receipt_dsh_$_ty
    , SUM(receipt_dsh_units_ty) AS receipt_dsh_units_ty
    , SUM(receipt_dsh_reg_$_ty) AS receipt_dsh_reg_$_ty
    , SUM(receipt_dsh_reg_units_ty) AS receipt_dsh_reg_units_ty
    , SUM(receipt_dsh_clr_$_ty) AS receipt_dsh_clr_$_ty
    , SUM(receipt_dsh_clr_units_ty) AS receipt_dsh_clr_units_ty
    --TY EOH
    , SUM(inventory_$_ty) AS inventory_$_ty
    , SUM(inventory_units_ty) AS inventory_units_ty
    , SUM(inventory_regpro_$_ty) AS inventory_regpro_$_ty
    , SUM(inventory_regpro_units_ty) AS inventory_regpro_units_ty
    , SUM(inventory_clr_$_ty) AS inventory_clr_$_ty
    , SUM(inventory_clr_units_ty) AS inventory_clr_units_ty
    --LY Sales
    , SUM(sales$_ly) AS sales$_ly
    , SUM(sales_units_ly) AS sales_units_ly
    , SUM(sale_profit_$_ly) AS sale_profit_$_ly
    , SUM(sale_reg_$_ly) AS sale_reg_$_ly
    , SUM(sale_reg_units_ly) AS sale_reg_units_ly
    , SUM(sale_pro_$_ly) AS sale_pro_$_ly
    , SUM(sale_pro_units_ly) AS sale_pro_units_ly
    , SUM(sale_clr_$_ly) AS sale_clr_$_ly
    , SUM(sale_clr_units_ly) AS sale_clr_units_ly
    , SUM(sale_bopis_$_ly) AS sale_bopis_$_ly
    , SUM(sale_bopis_units_ly) AS sale_bopis_units_ly
    , SUM(sale_dtc_$_ly) AS sale_dtc_$_ly
    , SUM(sale_dtc_units_ly) AS sale_dtc_units_ly
    , SUM(sale_dirff_$_ly) AS sale_dirff_$_ly
    , SUM(sale_dirff_units_ly) AS sale_dirff_units_ly
    , SUM(sale_strff_$_ly) AS sale_strff_$_ly
    , SUM(sale_strff_units_ly) AS sale_strff_units_ly
    --LY Returns
    , SUM(rtrn_$_ly) AS rtrn_$_ly
    , SUM(rtrn_units_ly) AS rtrn_units_ly
    , SUM(rtrn_reg_$_ly) AS rtrn_reg_$_ly
    , SUM(rtrn_reg_units_ly) AS rtrn_reg_units_ly
    , SUM(rtrn_pro_$_ly) AS rtrn_pro_$_ly
    , SUM(rtrn_pro_units_ly) AS rtrn_pro_units_ly
    , SUM(rtrn_clr_$_ly) AS rtrn_clr_$_ly
    , SUM(rtrn_clr_units_ly) AS rtrn_clr_units_ly
    --LY Demand
    , SUM(demand_$_ly) AS demand_$_ly
    , SUM(demand_units_ly) AS demand_units_ly
    , SUM(demand_reg_$_ly) AS demand_reg_$_ly
    , SUM(demand_reg_units_ly) AS demand_reg_units_ly
    , SUM(demand_pro_$_ly) AS demand_pro_$_ly
    , SUM(demand_pro_units_ly) AS demand_pro_units_ly
    , SUM(demand_clr_$_ly) AS demand_clr_$_ly
    , SUM(demand_clr_units_ly) AS demand_clr_units_ly
    , SUM(demand_ds_$_ly) AS demand_ds_$_ly
    , SUM(demand_ds_units_ly) AS demand_ds_units_ly
    , SUM(demand_fls_$_ly) AS demand_fls_$_ly
    , SUM(demand_fls_units_ly) AS demand_fls_units_ly
    , SUM(demand_fc_$_ly) AS demand_fc_$_ly
    , SUM(demand_fc_units_ly) AS demand_fc_units_ly
    --LY Ship
    , SUM(shipped_$_ly) AS shipped_$_ly
    , SUM(shipped_units_ly) AS shipped_units_ly
    , SUM(shipped_reg_$_ly) AS shipped_reg_$_ly
    , SUM(shipped_reg_units_ly) AS shipped_reg_units_ly
    , SUM(shipped_pro_$_ly) AS shipped_pro_$_ly
    , SUM(shipped_pro_units_ly) AS shipped_pro_units_ly
    , SUM(shipped_clr_$_ly) AS shipped_clr_$_ly
    , SUM(shipped_clr_units_ly) AS shipped_clr_units_ly
    , SUM(shipped_ds_$_ly) AS shipped_ds_$_ly
    , SUM(shipped_ds_units_ly) AS shipped_ds_units_ly
    , SUM(shipped_fls_$_ly) AS shipped_fls_$_ly
    , SUM(shipped_fls_units_ly) AS shipped_fls_units_ly
    , SUM(shipped_fc_$_ly) AS shipped_fc_$_ly
    , SUM(shipped_fc_units_ly) AS shipped_fc_units_ly
    --LY Receipts
    , SUM(receipt_$_ly) AS receipt_$_ly
    , SUM(receipt_units_ly) AS receipt_units_ly
    , SUM(receipt_po_$_ly) AS receipt_po_$_ly
    , SUM(receipt_po_units_ly) AS receipt_po_units_ly
    , SUM(receipt_po_reg_$_ly) AS receipt_po_reg_$_ly
    , SUM(receipt_po_reg_units_ly) AS receipt_po_reg_units_ly
    , SUM(receipt_po_clr_$_ly) AS receipt_po_clr_$_ly
    , SUM(receipt_po_clr_units_ly) AS receipt_po_clr_units_ly
    , SUM(receipt_rsk_$_ly) AS receipt_rsk_$_ly
    , SUM(receipt_rsk_units_ly) AS receipt_rsk_units_ly
    , SUM(receipt_rsk_reg_$_ly) AS receipt_rsk_reg_$_ly
    , SUM(receipt_rsk_reg_units_ly) AS receipt_rsk_reg_units_ly
    , SUM(receipt_rsk_clr_$_ly) AS receipt_rsk_clr_$_ly
    , SUM(receipt_rsk_clr_units_ly) AS receipt_rsk_clr_units_ly
    , SUM(receipt_dsh_$_ly) AS receipt_dsh_$_ly
    , SUM(receipt_dsh_units_ly) AS receipt_dsh_units_ly
    , SUM(receipt_dsh_reg_$_ly) AS receipt_dsh_reg_$_ly
    , SUM(receipt_dsh_reg_units_ly) AS receipt_dsh_reg_units_ly
    , SUM(receipt_dsh_clr_$_ly) AS receipt_dsh_clr_$_ly
    , SUM(receipt_dsh_clr_units_ly) AS receipt_dsh_clr_units_ly
    --LY EOH
    , SUM(inventory_$_ly) AS inventory_$_ly
    , SUM(inventory_units_ly) AS inventory_units_ly
    , SUM(inventory_regpro_$_ly) AS inventory_regpro_$_ly
    , SUM(inventory_regpro_units_ly) AS inventory_regpro_units_ly
    , SUM(inventory_clr_$_ly) AS inventory_clr_$_ly
    , SUM(inventory_clr_units_ly) AS inventory_clr_units_ly

FROM
(
    --BEGIN SALES AND DEMAND QUERY
        SELECT
            COALESCE(ty.sku_number, ly.sku_number) AS sku_number
            , COALESCE(ty.fiscal_week, ly.fiscal_week) AS fiscal_week
            , COALESCE(ty.fiscal_month, ly.fiscal_month) AS fiscal_month
            --TY Sales
            , SUM(COALESCE(sales$_ty,0)) AS sales$_ty
            , SUM(COALESCE(sales_units_ty,0)) AS sales_units_ty
            , SUM(COALESCE(sale_profit_$_ty,0)) AS sale_profit_$_ty
            , SUM(COALESCE(sale_reg_$_ty,0)) AS sale_reg_$_ty
            , SUM(COALESCE(sale_reg_units_ty,0)) AS sale_reg_units_ty
            , SUM(COALESCE(sale_pro_$_ty,0)) AS sale_pro_$_ty
            , SUM(COALESCE(sale_pro_units_ty,0)) AS sale_pro_units_ty
            , SUM(COALESCE(sale_clr_$_ty,0)) AS sale_clr_$_ty
            , SUM(COALESCE(sale_clr_units_ty,0)) AS sale_clr_units_ty
            , SUM(COALESCE(sale_bopis_$_ty,0)) AS sale_bopis_$_ty
            , SUM(COALESCE(sale_bopis_units_ty,0)) AS sale_bopis_units_ty
            , SUM(COALESCE(sale_dtc_$_ty,0)) AS sale_dtc_$_ty
            , SUM(COALESCE(sale_dtc_units_ty,0)) AS sale_dtc_units_ty
            , SUM(COALESCE(sale_dirff_$_ty,0)) AS sale_dirff_$_ty
            , SUM(COALESCE(sale_dirff_units_ty,0)) AS sale_dirff_units_ty
            , SUM(COALESCE(sale_strff_$_ty,0)) AS sale_strff_$_ty
            , SUM(COALESCE(sale_strff_units_ty,0)) AS sale_strff_units_ty
            --TY Returns
            , SUM(COALESCE(rtrn_$_ty,0)) AS rtrn_$_ty
            , SUM(COALESCE(rtrn_units_ty,0)) AS rtrn_units_ty
            , SUM(COALESCE(rtrn_reg_$_ty,0)) AS rtrn_reg_$_ty
            , SUM(COALESCE(rtrn_reg_units_ty,0)) AS rtrn_reg_units_ty
            , SUM(COALESCE(rtrn_pro_$_ty,0)) AS rtrn_pro_$_ty
            , SUM(COALESCE(rtrn_pro_units_ty,0)) AS rtrn_pro_units_ty
            , SUM(COALESCE(rtrn_clr_$_ty,0)) AS rtrn_clr_$_ty
            , SUM(COALESCE(rtrn_clr_units_ty,0)) AS rtrn_clr_units_ty
            --TY Demand
            , SUM(COALESCE(demand_$_ty,0)) AS demand_$_ty
            , SUM(COALESCE(demand_units_ty,0)) AS demand_units_ty
            , SUM(COALESCE(demand_reg_$_ty,0)) AS demand_reg_$_ty
            , SUM(COALESCE(demand_reg_units_ty,0)) AS demand_reg_units_ty
            , SUM(COALESCE(demand_pro_$_ty,0)) AS demand_pro_$_ty
            , SUM(COALESCE(demand_pro_units_ty,0)) AS demand_pro_units_ty
            , SUM(COALESCE(demand_clr_$_ty,0)) AS demand_clr_$_ty
            , SUM(COALESCE(demand_clr_units_ty,0)) AS demand_clr_units_ty
            , SUM(COALESCE(demand_ds_$_ty,0)) AS demand_ds_$_ty
            , SUM(COALESCE(demand_ds_units_ty,0)) AS demand_ds_units_ty
            , SUM(COALESCE(demand_fls_$_ty,0)) AS demand_fls_$_ty
            , SUM(COALESCE(demand_fls_units_ty,0)) AS demand_fls_units_ty
            , SUM(COALESCE(demand_fc_$_ty,0)) AS demand_fc_$_ty
            , SUM(COALESCE(demand_fc_units_ty,0)) AS demand_fc_units_ty
            --TY Ship
            , SUM(COALESCE(shipped_$_ty,0)) AS shipped_$_ty
            , SUM(COALESCE(shipped_units_ty,0)) AS shipped_units_ty
            , SUM(COALESCE(shipped_reg_$_ty,0)) AS shipped_reg_$_ty
            , SUM(COALESCE(shipped_reg_units_ty,0)) AS shipped_reg_units_ty
            , SUM(COALESCE(shipped_pro_$_ty,0)) AS shipped_pro_$_ty
            , SUM(COALESCE(shipped_pro_units_ty,0)) AS shipped_pro_units_ty
            , SUM(COALESCE(shipped_clr_$_ty,0)) AS shipped_clr_$_ty
            , SUM(COALESCE(shipped_clr_units_ty,0)) AS shipped_clr_units_ty
            , SUM(COALESCE(shipped_ds_$_ty,0)) AS shipped_ds_$_ty
            , SUM(COALESCE(shipped_ds_units_ty,0)) AS shipped_ds_units_ty
            , SUM(COALESCE(shipped_fls_$_ty,0)) AS shipped_fls_$_ty
            , SUM(COALESCE(shipped_fls_units_ty,0)) AS shipped_fls_units_ty
            , SUM(COALESCE(shipped_fc_$_ty,0)) AS shipped_fc_$_ty
            , SUM(COALESCE(shipped_fc_units_ty,0)) AS shipped_fc_units_ty
            --TY Receipts
            , CAST(0 as DECIMAL(20,2)) AS receipt_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_po_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_po_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_po_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_po_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_po_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_po_clr_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_rsk_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_rsk_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_rsk_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_rsk_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_rsk_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_rsk_clr_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_dsh_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_dsh_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_dsh_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_dsh_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_dsh_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_dsh_clr_units_ty
            --TY EOH
            , CAST(0 as DECIMAL(20,2)) AS inventory_$_ty
            , CAST(0 as DECIMAL(12,0)) AS inventory_units_ty
            , CAST(0 as DECIMAL(20,2)) AS inventory_regpro_$_ty
            , CAST(0 as DECIMAL(12,0)) AS inventory_regpro_units_ty
            , CAST(0 as DECIMAL(20,2)) AS inventory_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS inventory_clr_units_ty
            --LY Sales
            , SUM(COALESCE(sales$_ly,0)) AS sales$_ly
            , SUM(COALESCE(sales_units_ly,0)) AS sales_units_ly
            , SUM(COALESCE(sale_profit_$_ly,0)) AS sale_profit_$_ly
            , SUM(COALESCE(sale_reg_$_ly,0)) AS sale_reg_$_ly
            , SUM(COALESCE(sale_reg_units_ly,0)) AS sale_reg_units_ly
            , SUM(COALESCE(sale_pro_$_ly,0)) AS sale_pro_$_ly
            , SUM(COALESCE(sale_pro_units_ly,0)) AS sale_pro_units_ly
            , SUM(COALESCE(sale_clr_$_ly,0)) AS sale_clr_$_ly
            , SUM(COALESCE(sale_clr_units_ly,0)) AS sale_clr_units_ly
            , SUM(COALESCE(sale_bopis_$_ly,0)) AS sale_bopis_$_ly
            , SUM(COALESCE(sale_bopis_units_ly,0)) AS sale_bopis_units_ly
            , SUM(COALESCE(sale_dtc_$_ly,0)) AS sale_dtc_$_ly
            , SUM(COALESCE(sale_dtc_units_ly,0)) AS sale_dtc_units_ly
            , SUM(COALESCE(sale_dirff_$_ly,0)) AS sale_dirff_$_ly
            , SUM(COALESCE(sale_dirff_units_ly,0)) AS sale_dirff_units_ly
            , SUM(COALESCE(sale_strff_$_ly,0)) AS sale_strff_$_ly
            , SUM(COALESCE(sale_strff_units_ly,0)) AS sale_strff_units_ly
            --LY Returns
            , SUM(COALESCE(rtrn_$_ly,0)) AS rtrn_$_ly
            , SUM(COALESCE(rtrn_units_ly,0)) AS rtrn_units_ly
            , SUM(COALESCE(rtrn_reg_$_ly,0)) AS rtrn_reg_$_ly
            , SUM(COALESCE(rtrn_reg_units_ly,0)) AS rtrn_reg_units_ly
            , SUM(COALESCE(rtrn_pro_$_ly,0)) AS rtrn_pro_$_ly
            , SUM(COALESCE(rtrn_pro_units_ly,0)) AS rtrn_pro_units_ly
            , SUM(COALESCE(rtrn_clr_$_ly,0)) AS rtrn_clr_$_ly
            , SUM(COALESCE(rtrn_clr_units_ly,0)) AS rtrn_clr_units_ly
            --LY Demand
            , SUM(COALESCE(demand_$_ly,0)) AS demand_$_ly
            , SUM(COALESCE(demand_units_ly,0)) AS demand_units_ly
            , SUM(COALESCE(demand_reg_$_ly,0)) AS demand_reg_$_ly
            , SUM(COALESCE(demand_reg_units_ly,0)) AS demand_reg_units_ly
            , SUM(COALESCE(demand_pro_$_ly,0)) AS demand_pro_$_ly
            , SUM(COALESCE(demand_pro_units_ly,0)) AS demand_pro_units_ly
            , SUM(COALESCE(demand_clr_$_ly,0)) AS demand_clr_$_ly
            , SUM(COALESCE(demand_clr_units_ly,0)) AS demand_clr_units_ly
            , SUM(COALESCE(demand_ds_$_ly,0)) AS demand_ds_$_ly
            , SUM(COALESCE(demand_ds_units_ly,0)) AS demand_ds_units_ly
            , SUM(COALESCE(demand_fls_$_ly,0)) AS demand_fls_$_ly
            , SUM(COALESCE(demand_fls_units_ly,0)) AS demand_fls_units_ly
            , SUM(COALESCE(demand_fc_$_ly,0)) AS demand_fc_$_ly
            , SUM(COALESCE(demand_fc_units_ly,0)) AS demand_fc_units_ly
            --LY Ship
            , SUM(COALESCE(shipped_$_ly,0)) AS shipped_$_ly
            , SUM(COALESCE(shipped_units_ly,0)) AS shipped_units_ly
            , SUM(COALESCE(shipped_reg_$_ly,0)) AS shipped_reg_$_ly
            , SUM(COALESCE(shipped_reg_units_ly,0)) AS shipped_reg_units_ly
            , SUM(COALESCE(shipped_pro_$_ly,0)) AS shipped_pro_$_ly
            , SUM(COALESCE(shipped_pro_units_ly,0)) AS shipped_pro_units_ly
            , SUM(COALESCE(shipped_clr_$_ly,0)) AS shipped_clr_$_ly
            , SUM(COALESCE(shipped_clr_units_ly,0)) AS shipped_clr_units_ly
            , SUM(COALESCE(shipped_ds_$_ly,0)) AS shipped_ds_$_ly
            , SUM(COALESCE(shipped_ds_units_ly,0)) AS shipped_ds_units_ly
            , SUM(COALESCE(shipped_fls_$_ly,0)) AS shipped_fls_$_ly
            , SUM(COALESCE(shipped_fls_units_ly,0)) AS shipped_fls_units_ly
            , SUM(COALESCE(shipped_fc_$_ly,0)) AS shipped_fc_$_ly
            , SUM(COALESCE(shipped_fc_units_ly,0)) AS shipped_fc_units_ly
            --LY Receipts
            , CAST(0 as DECIMAL(20,2)) AS receipt_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_po_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_po_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_po_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_po_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_po_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_po_clr_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_rsk_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_rsk_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_rsk_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_rsk_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_rsk_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_rsk_clr_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_dsh_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_dsh_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_dsh_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_dsh_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_dsh_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_dsh_clr_units_ly
            --LY EOH
            , CAST(0 as DECIMAL(20,2)) AS inventory_$_ly
            , CAST(0 as DECIMAL(12,0)) AS inventory_units_ly
            , CAST(0 as DECIMAL(20,2)) AS inventory_regpro_$_ly
            , CAST(0 as DECIMAL(12,0)) AS inventory_regpro_units_ly
            , CAST(0 as DECIMAL(20,2)) AS inventory_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS inventory_clr_units_ly

        FROM
        (
            SELECT
                fact.sku_number
                , weeks.fiscal_week
                , weeks.fiscal_month
                --TY Sales
                , SUM(sales$) AS sales$_ty
                , SUM(sales_units) AS sales_units_ty
                , SUM(sale_profit_$) AS sale_profit_$_ty
                , SUM(sale_reg_$) AS sale_reg_$_ty
                , SUM(sale_reg_units) AS sale_reg_units_ty
                , SUM(sale_pro_$) AS sale_pro_$_ty
                , SUM(sale_pro_units) AS sale_pro_units_ty
                , SUM(sale_clr_$) AS sale_clr_$_ty
                , SUM(sale_clr_units) AS sale_clr_units_ty
                , SUM(sale_bopis_$) AS sale_bopis_$_ty
                , SUM(sale_bopis_units) AS sale_bopis_units_ty
                , SUM(sale_dtc_$) AS sale_dtc_$_ty
                , SUM(sale_dtc_units) AS sale_dtc_units_ty
                , SUM(sale_dirff_$) AS sale_dirff_$_ty
                , SUM(sale_dirff_units) AS sale_dirff_units_ty
                , SUM(sale_strff_$) AS sale_strff_$_ty
                , SUM(sale_strff_units) AS sale_strff_units_ty
                --TY Returns
                , SUM(rtrn_$) AS rtrn_$_ty
                , SUM(rtrn_units) AS rtrn_units_ty
                , SUM(rtrn_reg_$) AS rtrn_reg_$_ty
                , SUM(rtrn_reg_units) AS rtrn_reg_units_ty
                , SUM(rtrn_pro_$) AS rtrn_pro_$_ty
                , SUM(rtrn_pro_units) AS rtrn_pro_units_ty
                , SUM(rtrn_clr_$) AS rtrn_clr_$_ty
                , SUM(rtrn_clr_units) AS rtrn_clr_units_ty
                --TY Demand
                , SUM(demand_$) AS demand_$_ty
                , SUM(demand_units) AS demand_units_ty
                , SUM(demand_reg_$) AS demand_reg_$_ty
                , SUM(demand_reg_units) AS demand_reg_units_ty
                , SUM(demand_pro_$) AS demand_pro_$_ty
                , SUM(demand_pro_units) AS demand_pro_units_ty
                , SUM(demand_clr_$) AS demand_clr_$_ty
                , SUM(demand_clr_units) AS demand_clr_units_ty
                , SUM(demand_ds_$) AS demand_ds_$_ty
                , SUM(demand_ds_units) AS demand_ds_units_ty
                , SUM(demand_fls_$) AS demand_fls_$_ty
                , SUM(demand_fls_units) AS demand_fls_units_ty
                , SUM(demand_fc_$) AS demand_fc_$_ty
                , SUM(demand_fc_units) AS demand_fc_units_ty
                --TY Ship
                , SUM(shipped_$) AS shipped_$_ty
                , SUM(shipped_units) AS shipped_units_ty
                , SUM(shipped_reg_$) AS shipped_reg_$_ty
                , SUM(shipped_reg_units) AS shipped_reg_units_ty
                , SUM(shipped_pro_$) AS shipped_pro_$_ty
                , SUM(shipped_pro_units) AS shipped_pro_units_ty
                , SUM(shipped_clr_$) AS shipped_clr_$_ty
                , SUM(shipped_clr_units) AS shipped_clr_units_ty
                , SUM(shipped_ds_$) AS shipped_ds_$_ty
                , SUM(shipped_ds_units) AS shipped_ds_units_ty
                , SUM(shipped_fls_$) AS shipped_fls_$_ty
                , SUM(shipped_fls_units) AS shipped_fls_units_ty
                , SUM(shipped_fc_$) AS shipped_fc_$_ty
                , SUM(shipped_fc_units) AS shipped_fc_units_ty

            FROM
                sale_dmnd_sku_lw fact
                INNER JOIN skus
                    ON fact.sku_number = skus.sku_number
                INNER JOIN locations
                    ON fact.location = locations.location
                INNER JOIN weeks
                    ON fact.fiscal_week = weeks.fiscal_week

            WHERE
                fact.fiscal_week BETWEEN (SELECT start_fiscal_week FROM week_range) AND (SELECT end_fiscal_week FROM week_range)
                AND fact.channel_number = 120

            GROUP BY
                fact.sku_number
                , weeks.fiscal_week
                , weeks.fiscal_month
        ) ty
        FULL OUTER JOIN
        (
            SELECT
                fact.sku_number
                , weeks.next_yr_fiscal_week AS fiscal_week --magic
                , weeks.next_yr_fiscal_month AS fiscal_month --magic
                --LY Sales
                , SUM(sales$) AS sales$_ly
                , SUM(sales_units) AS sales_units_ly
                , SUM(sale_profit_$) AS sale_profit_$_ly
                , SUM(sale_reg_$) AS sale_reg_$_ly
                , SUM(sale_reg_units) AS sale_reg_units_ly
                , SUM(sale_pro_$) AS sale_pro_$_ly
                , SUM(sale_pro_units) AS sale_pro_units_ly
                , SUM(sale_clr_$) AS sale_clr_$_ly
                , SUM(sale_clr_units) AS sale_clr_units_ly
                , SUM(sale_bopis_$) AS sale_bopis_$_ly
                , SUM(sale_bopis_units) AS sale_bopis_units_ly
                , SUM(sale_dtc_$) AS sale_dtc_$_ly
                , SUM(sale_dtc_units) AS sale_dtc_units_ly
                , SUM(sale_dirff_$) AS sale_dirff_$_ly
                , SUM(sale_dirff_units) AS sale_dirff_units_ly
                , SUM(sale_strff_$) AS sale_strff_$_ly
                , SUM(sale_strff_units) AS sale_strff_units_ly
                --LY Returns
                , SUM(rtrn_$) AS rtrn_$_ly
                , SUM(rtrn_units) AS rtrn_units_ly
                , SUM(rtrn_reg_$) AS rtrn_reg_$_ly
                , SUM(rtrn_reg_units) AS rtrn_reg_units_ly
                , SUM(rtrn_pro_$) AS rtrn_pro_$_ly
                , SUM(rtrn_pro_units) AS rtrn_pro_units_ly
                , SUM(rtrn_clr_$) AS rtrn_clr_$_ly
                , SUM(rtrn_clr_units) AS rtrn_clr_units_ly
                --LY Demand
                , SUM(demand_$) AS demand_$_ly
                , SUM(demand_units) AS demand_units_ly
                , SUM(demand_reg_$) AS demand_reg_$_ly
                , SUM(demand_reg_units) AS demand_reg_units_ly
                , SUM(demand_pro_$) AS demand_pro_$_ly
                , SUM(demand_pro_units) AS demand_pro_units_ly
                , SUM(demand_clr_$) AS demand_clr_$_ly
                , SUM(demand_clr_units) AS demand_clr_units_ly
                , SUM(demand_ds_$) AS demand_ds_$_ly
                , SUM(demand_ds_units) AS demand_ds_units_ly
                , SUM(demand_fls_$) AS demand_fls_$_ly
                , SUM(demand_fls_units) AS demand_fls_units_ly
                , SUM(demand_fc_$) AS demand_fc_$_ly
                , SUM(demand_fc_units) AS demand_fc_units_ly
                --LY Ship
                , SUM(shipped_$) AS shipped_$_ly
                , SUM(shipped_units) AS shipped_units_ly
                , SUM(shipped_reg_$) AS shipped_reg_$_ly
                , SUM(shipped_reg_units) AS shipped_reg_units_ly
                , SUM(shipped_pro_$) AS shipped_pro_$_ly
                , SUM(shipped_pro_units) AS shipped_pro_units_ly
                , SUM(shipped_clr_$) AS shipped_clr_$_ly
                , SUM(shipped_clr_units) AS shipped_clr_units_ly
                , SUM(shipped_ds_$) AS shipped_ds_$_ly
                , SUM(shipped_ds_units) AS shipped_ds_units_ly
                , SUM(shipped_fls_$) AS shipped_fls_$_ly
                , SUM(shipped_fls_units) AS shipped_fls_units_ly
                , SUM(shipped_fc_$) AS shipped_fc_$_ly
                , SUM(shipped_fc_units) AS shipped_fc_units_ly

            FROM
                sale_dmnd_sku_lw fact
                INNER JOIN skus
                    ON fact.sku_number = skus.sku_number
                INNER JOIN locations
                    ON fact.location = locations.location
                INNER JOIN weeks
                    ON fact.fiscal_week = weeks.fiscal_week

            --magic
            WHERE weeks.next_yr_fiscal_week BETWEEN (SELECT start_fiscal_week FROM week_range) AND (SELECT end_fiscal_week FROM week_range)

            GROUP BY
                fact.sku_number
                , weeks.next_yr_fiscal_week
                , weeks.next_yr_fiscal_month
        ) ly
            ON ty.sku_number = ly.sku_number
            AND ty.fiscal_week = ly.fiscal_week
            AND ty.fiscal_month = ly.fiscal_month

        GROUP BY
            COALESCE(ty.sku_number, ly.sku_number)
            , COALESCE(ty.fiscal_week, ly.fiscal_week)
            , COALESCE(ty.fiscal_month, ly.fiscal_month)
    --END SALES AND DEMAND QUERY

    UNION ALL

    --BEGIN RECEIPTS QUERY
        SELECT
            COALESCE(ty.sku_number, ly.sku_number) AS sku_number
            , COALESCE(ty.fiscal_week, ly.fiscal_week) AS fiscal_week
            , COALESCE(ty.fiscal_month, ly.fiscal_month) AS fiscal_month
            --TY Sales
            , CAST(0 as DECIMAL(20,2)) AS sales$_ty
            , CAST(0 as DECIMAL(12,0)) AS sales_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_profit_$_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_pro_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_pro_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_clr_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_bopis_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_bopis_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_dtc_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_dtc_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_dirff_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_dirff_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_strff_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_strff_units_ty
            --TY Returns
            , CAST(0 as DECIMAL(20,2)) AS rtrn_$_ty
            , CAST(0 as DECIMAL(12,0)) AS rtrn_units_ty
            , CAST(0 as DECIMAL(20,2)) AS rtrn_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS rtrn_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS rtrn_pro_$_ty
            , CAST(0 as DECIMAL(12,0)) AS rtrn_pro_units_ty
            , CAST(0 as DECIMAL(20,2)) AS rtrn_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS rtrn_clr_units_ty
            --TY Demand
            , CAST(0 as DECIMAL(20,2)) AS demand_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_units_ty
            , CAST(0 as DECIMAL(20,2)) AS demand_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS demand_pro_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_pro_units_ty
            , CAST(0 as DECIMAL(20,2)) AS demand_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_clr_units_ty
            , CAST(0 as DECIMAL(20,2)) AS demand_ds_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_ds_units_ty
            , CAST(0 as DECIMAL(20,2)) AS demand_fls_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_fls_units_ty
            , CAST(0 as DECIMAL(20,2)) AS demand_fc_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_fc_units_ty
            --TY Ship
            , CAST(0 as DECIMAL(20,2)) AS shipped_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_units_ty
            , CAST(0 as DECIMAL(20,2)) AS shipped_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS shipped_pro_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_pro_units_ty
            , CAST(0 as DECIMAL(20,2)) AS shipped_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_clr_units_ty
            , CAST(0 as DECIMAL(20,2)) AS shipped_ds_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_ds_units_ty
            , CAST(0 as DECIMAL(20,2)) AS shipped_fls_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_fls_units_ty
            , CAST(0 as DECIMAL(20,2)) AS shipped_fc_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_fc_units_ty
            --TY Receipts
            , SUM(COALESCE(receipt_$_ty,0)) AS receipt_$_ty
            , SUM(COALESCE(receipt_units_ty,0)) AS receipt_units_ty
            , SUM(COALESCE(receipt_po_$_ty,0)) AS receipt_po_$_ty
            , SUM(COALESCE(receipt_po_units_ty,0)) AS receipt_po_units_ty
            , SUM(COALESCE(receipt_po_reg_$_ty,0)) AS receipt_po_reg_$_ty
            , SUM(COALESCE(receipt_po_reg_units_ty,0)) AS receipt_po_reg_units_ty
            , SUM(COALESCE(receipt_po_clr_$_ty,0)) AS receipt_po_clr_$_ty
            , SUM(COALESCE(receipt_po_clr_units_ty,0)) AS receipt_po_clr_units_ty
            , SUM(COALESCE(receipt_rsk_$_ty,0)) AS receipt_rsk_$_ty
            , SUM(COALESCE(receipt_rsk_units_ty,0)) AS receipt_rsk_units_ty
            , SUM(COALESCE(receipt_rsk_reg_$_ty,0)) AS receipt_rsk_reg_$_ty
            , SUM(COALESCE(receipt_rsk_reg_units_ty,0)) AS receipt_rsk_reg_units_ty
            , SUM(COALESCE(receipt_rsk_clr_$_ty,0)) AS receipt_rsk_clr_$_ty
            , SUM(COALESCE(receipt_rsk_clr_units_ty,0)) AS receipt_rsk_clr_units_ty
            , SUM(COALESCE(receipt_dsh_$_ty,0)) AS receipt_dsh_$_ty
            , SUM(COALESCE(receipt_dsh_units_ty,0)) AS receipt_dsh_units_ty
            , SUM(COALESCE(receipt_dsh_reg_$_ty,0)) AS receipt_dsh_reg_$_ty
            , SUM(COALESCE(receipt_dsh_reg_units_ty,0)) AS receipt_dsh_reg_units_ty
            , SUM(COALESCE(receipt_dsh_clr_$_ty,0)) AS receipt_dsh_clr_$_ty
            , SUM(COALESCE(receipt_dsh_clr_units_ty,0)) AS receipt_dsh_clr_units_ty
            --TY EOH
            , CAST(0 as DECIMAL(20,2)) AS inventory_$_ty
            , CAST(0 as DECIMAL(12,0)) AS inventory_units_ty
            , CAST(0 as DECIMAL(20,2)) AS inventory_regpro_$_ty
            , CAST(0 as DECIMAL(12,0)) AS inventory_regpro_units_ty
            , CAST(0 as DECIMAL(20,2)) AS inventory_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS inventory_clr_units_ty
            --LY Sales
            , CAST(0 as DECIMAL(20,2)) AS sales$_ly
            , CAST(0 as DECIMAL(12,0)) AS sales_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_profit_$_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_pro_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_pro_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_clr_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_bopis_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_bopis_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_dtc_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_dtc_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_dirff_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_dirff_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_strff_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_strff_units_ly
            --LY Returns
            , CAST(0 as DECIMAL(20,2)) AS rtrn_$_ly
            , CAST(0 as DECIMAL(12,0)) AS rtrn_units_ly
            , CAST(0 as DECIMAL(20,2)) AS rtrn_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS rtrn_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS rtrn_pro_$_ly
            , CAST(0 as DECIMAL(12,0)) AS rtrn_pro_units_ly
            , CAST(0 as DECIMAL(20,2)) AS rtrn_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS rtrn_clr_units_ly
            --LY Demand
            , CAST(0 as DECIMAL(20,2)) AS demand_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_units_ly
            , CAST(0 as DECIMAL(20,2)) AS demand_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS demand_pro_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_pro_units_ly
            , CAST(0 as DECIMAL(20,2)) AS demand_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_clr_units_ly
            , CAST(0 as DECIMAL(20,2)) AS demand_ds_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_ds_units_ly
            , CAST(0 as DECIMAL(20,2)) AS demand_fls_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_fls_units_ly
            , CAST(0 as DECIMAL(20,2)) AS demand_fc_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_fc_units_ly
            --LY Ship
            , CAST(0 as DECIMAL(20,2)) AS shipped_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_units_ly
            , CAST(0 as DECIMAL(20,2)) AS shipped_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS shipped_pro_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_pro_units_ly
            , CAST(0 as DECIMAL(20,2)) AS shipped_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_clr_units_ly
            , CAST(0 as DECIMAL(20,2)) AS shipped_ds_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_ds_units_ly
            , CAST(0 as DECIMAL(20,2)) AS shipped_fls_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_fls_units_ly
            , CAST(0 as DECIMAL(20,2)) AS shipped_fc_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_fc_units_ly
            --LY Receipts
            , SUM(COALESCE(receipt_$_ly,0)) AS receipt_$_ly
            , SUM(COALESCE(receipt_units_ly,0)) AS receipt_units_ly
            , SUM(COALESCE(receipt_po_$_ly,0)) AS receipt_po_$_ly
            , SUM(COALESCE(receipt_po_units_ly,0)) AS receipt_po_units_ly
            , SUM(COALESCE(receipt_po_reg_$_ly,0)) AS receipt_po_reg_$_ly
            , SUM(COALESCE(receipt_po_reg_units_ly,0)) AS receipt_po_reg_units_ly
            , SUM(COALESCE(receipt_po_clr_$_ly,0)) AS receipt_po_clr_$_ly
            , SUM(COALESCE(receipt_po_clr_units_ly,0)) AS receipt_po_clr_units_ly
            , SUM(COALESCE(receipt_rsk_$_ly,0)) AS receipt_rsk_$_ly
            , SUM(COALESCE(receipt_rsk_units_ly,0)) AS receipt_rsk_units_ly
            , SUM(COALESCE(receipt_rsk_reg_$_ly,0)) AS receipt_rsk_reg_$_ly
            , SUM(COALESCE(receipt_rsk_reg_units_ly,0)) AS receipt_rsk_reg_units_ly
            , SUM(COALESCE(receipt_rsk_clr_$_ly,0)) AS receipt_rsk_clr_$_ly
            , SUM(COALESCE(receipt_rsk_clr_units_ly,0)) AS receipt_rsk_clr_units_ly
            , SUM(COALESCE(receipt_dsh_$_ly,0)) AS receipt_dsh_$_ly
            , SUM(COALESCE(receipt_dsh_units_ly,0)) AS receipt_dsh_units_ly
            , SUM(COALESCE(receipt_dsh_reg_$_ly,0)) AS receipt_dsh_reg_$_ly
            , SUM(COALESCE(receipt_dsh_reg_units_ly,0)) AS receipt_dsh_reg_units_ly
            , SUM(COALESCE(receipt_dsh_clr_$_ly,0)) AS receipt_dsh_clr_$_ly
            , SUM(COALESCE(receipt_dsh_clr_units_ly,0)) AS receipt_dsh_clr_units_ly
            --LY EOH
            , CAST(0 as DECIMAL(20,2)) AS inventory_$_ly
            , CAST(0 as DECIMAL(12,0)) AS inventory_units_ly
            , CAST(0 as DECIMAL(20,2)) AS inventory_regpro_$_ly
            , CAST(0 as DECIMAL(12,0)) AS inventory_regpro_units_ly
            , CAST(0 as DECIMAL(20,2)) AS inventory_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS inventory_clr_units_ly

        FROM
        (
            SELECT
                fact.sku_number
                , fact.fiscal_week
                , fact.fiscal_month
                , SUM(receipt_po_$ + receipt_rsk_$ + receipt_dsh_$) AS receipt_$_ty
                , SUM(receipt_po_units + receipt_rsk_units + receipt_dsh_units) AS receipt_units_ty
                , SUM(receipt_po_$) AS receipt_po_$_ty
                , SUM(receipt_po_units) AS receipt_po_units_ty
                , SUM(receipt_po_reg_$) AS receipt_po_reg_$_ty
                , SUM(receipt_po_reg_units) AS receipt_po_reg_units_ty
                , SUM(receipt_po_clr_$) AS receipt_po_clr_$_ty
                , SUM(receipt_po_clr_units) AS receipt_po_clr_units_ty
                , SUM(receipt_rsk_$) AS receipt_rsk_$_ty
                , SUM(receipt_rsk_units) AS receipt_rsk_units_ty
                , SUM(receipt_rsk_reg_$) AS receipt_rsk_reg_$_ty
                , SUM(receipt_rsk_reg_units) AS receipt_rsk_reg_units_ty
                , SUM(receipt_rsk_clr_$) AS receipt_rsk_clr_$_ty
                , SUM(receipt_rsk_clr_units) AS receipt_rsk_clr_units_ty
                , SUM(receipt_dsh_$) AS receipt_dsh_$_ty
                , SUM(receipt_dsh_units) AS receipt_dsh_units_ty
                , SUM(receipt_dsh_reg_$) AS receipt_dsh_reg_$_ty
                , SUM(receipt_dsh_reg_units) AS receipt_dsh_reg_units_ty
                , SUM(receipt_dsh_clr_$) AS receipt_dsh_clr_$_ty
                , SUM(receipt_dsh_clr_units) AS receipt_dsh_clr_units_ty

            FROM
                receipt_sku_cw fact
                INNER JOIN skus
                    ON fact.sku_number = skus.sku_number
                INNER JOIN weeks
                    ON fact.fiscal_week = weeks.fiscal_week

            WHERE
                fact.fiscal_week BETWEEN (SELECT start_fiscal_week FROM week_range) AND (SELECT end_fiscal_week FROM week_range)
                AND fact.channel_number = 120

            GROUP BY
                fact.sku_number
                , fact.fiscal_week
                , fact.fiscal_month
        ) ty
        FULL OUTER JOIN
        (
            SELECT
                fact.sku_number
                , weeks.next_yr_fiscal_week AS fiscal_week --magic
                , weeks.next_yr_fiscal_month AS fiscal_month --magic
                , SUM(receipt_po_$ + receipt_rsk_$ + receipt_dsh_$) AS receipt_$_ly
                , SUM(receipt_po_units + receipt_rsk_units + receipt_dsh_units) AS receipt_units_ly
                , SUM(receipt_po_$) AS receipt_po_$_ly
                , SUM(receipt_po_units) AS receipt_po_units_ly
                , SUM(receipt_po_reg_$) AS receipt_po_reg_$_ly
                , SUM(receipt_po_reg_units) AS receipt_po_reg_units_ly
                , SUM(receipt_po_clr_$) AS receipt_po_clr_$_ly
                , SUM(receipt_po_clr_units) AS receipt_po_clr_units_ly
                , SUM(receipt_rsk_$) AS receipt_rsk_$_ly
                , SUM(receipt_rsk_units) AS receipt_rsk_units_ly
                , SUM(receipt_rsk_reg_$) AS receipt_rsk_reg_$_ly
                , SUM(receipt_rsk_reg_units) AS receipt_rsk_reg_units_ly
                , SUM(receipt_rsk_clr_$) AS receipt_rsk_clr_$_ly
                , SUM(receipt_rsk_clr_units) AS receipt_rsk_clr_units_ly
                , SUM(receipt_dsh_$) AS receipt_dsh_$_ly
                , SUM(receipt_dsh_units) AS receipt_dsh_units_ly
                , SUM(receipt_dsh_reg_$) AS receipt_dsh_reg_$_ly
                , SUM(receipt_dsh_reg_units) AS receipt_dsh_reg_units_ly
                , SUM(receipt_dsh_clr_$) AS receipt_dsh_clr_$_ly
                , SUM(receipt_dsh_clr_units) AS receipt_dsh_clr_units_ly

            FROM
                receipt_sku_cw fact
                INNER JOIN skus
                    ON fact.sku_number = skus.sku_number
                INNER JOIN weeks
                    ON fact.fiscal_week = weeks.fiscal_week

            WHERE
                --magic
                weeks.next_yr_fiscal_week BETWEEN (SELECT start_fiscal_week FROM week_range) AND (SELECT end_fiscal_week FROM week_range)
                AND fact.channel_number = 120

            GROUP BY
                fact.sku_number
                , weeks.next_yr_fiscal_week
                , weeks.next_yr_fiscal_month
        ) ly
            ON ty.sku_number = ly.sku_number
            AND ty.fiscal_week = ly.fiscal_week
            AND ty.fiscal_month = ly.fiscal_month

        GROUP BY
            COALESCE(ty.sku_number, ly.sku_number)
            , COALESCE(ty.fiscal_week, ly.fiscal_week)
            , COALESCE(ty.fiscal_month, ly.fiscal_month)
    --END RECEIPTS QUERY

    UNION ALL

    --BEGIN INVENTORY QUERY
        SELECT
            COALESCE(ty.sku_number, ly.sku_number) AS sku_number
            , COALESCE(ty.fiscal_week, ly.fiscal_week) AS fiscal_week
            , COALESCE(ty.fiscal_month, ly.fiscal_month) AS fiscal_month
            --TY Sales
            , CAST(0 as DECIMAL(20,2)) AS sales$_ty
            , CAST(0 as DECIMAL(12,0)) AS sales_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_profit_$_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_pro_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_pro_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_clr_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_bopis_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_bopis_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_dtc_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_dtc_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_dirff_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_dirff_units_ty
            , CAST(0 as DECIMAL(20,2)) AS sale_strff_$_ty
            , CAST(0 as DECIMAL(12,0)) AS sale_strff_units_ty
            --TY Returns
            , CAST(0 as DECIMAL(20,2)) AS rtrn_$_ty
            , CAST(0 as DECIMAL(12,0)) AS rtrn_units_ty
            , CAST(0 as DECIMAL(20,2)) AS rtrn_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS rtrn_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS rtrn_pro_$_ty
            , CAST(0 as DECIMAL(12,0)) AS rtrn_pro_units_ty
            , CAST(0 as DECIMAL(20,2)) AS rtrn_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS rtrn_clr_units_ty
            --TY Demand
            , CAST(0 as DECIMAL(20,2)) AS demand_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_units_ty
            , CAST(0 as DECIMAL(20,2)) AS demand_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS demand_pro_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_pro_units_ty
            , CAST(0 as DECIMAL(20,2)) AS demand_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_clr_units_ty
            , CAST(0 as DECIMAL(20,2)) AS demand_ds_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_ds_units_ty
            , CAST(0 as DECIMAL(20,2)) AS demand_fls_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_fls_units_ty
            , CAST(0 as DECIMAL(20,2)) AS demand_fc_$_ty
            , CAST(0 as DECIMAL(12,0)) AS demand_fc_units_ty
            --TY Ship
            , CAST(0 as DECIMAL(20,2)) AS shipped_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_units_ty
            , CAST(0 as DECIMAL(20,2)) AS shipped_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS shipped_pro_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_pro_units_ty
            , CAST(0 as DECIMAL(20,2)) AS shipped_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_clr_units_ty
            , CAST(0 as DECIMAL(20,2)) AS shipped_ds_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_ds_units_ty
            , CAST(0 as DECIMAL(20,2)) AS shipped_fls_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_fls_units_ty
            , CAST(0 as DECIMAL(20,2)) AS shipped_fc_$_ty
            , CAST(0 as DECIMAL(12,0)) AS shipped_fc_units_ty
            --TY Receipts
            , CAST(0 as DECIMAL(20,2)) AS receipt_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_po_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_po_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_po_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_po_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_po_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_po_clr_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_rsk_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_rsk_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_rsk_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_rsk_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_rsk_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_rsk_clr_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_dsh_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_dsh_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_dsh_reg_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_dsh_reg_units_ty
            , CAST(0 as DECIMAL(20,2)) AS receipt_dsh_clr_$_ty
            , CAST(0 as DECIMAL(12,0)) AS receipt_dsh_clr_units_ty
            --TY EOH
            , SUM(COALESCE(inventory_$_ty,0)) AS inventory_$_ty
            , SUM(COALESCE(inventory_units_ty,0)) AS inventory_units_ty
            , SUM(COALESCE(inventory_regpro_$_ty,0)) AS inventory_regpro_$_ty
            , SUM(COALESCE(inventory_regpro_units_ty,0)) AS inventory_regpro_units_ty
            , SUM(COALESCE(inventory_clr_$_ty,0)) AS inventory_clr_$_ty
            , SUM(COALESCE(inventory_clr_units_ty,0)) AS inventory_clr_units_ty
            --LY Sales
            , CAST(0 as DECIMAL(20,2)) AS sales$_ly
            , CAST(0 as DECIMAL(12,0)) AS sales_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_profit_$_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_pro_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_pro_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_clr_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_bopis_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_bopis_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_dtc_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_dtc_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_dirff_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_dirff_units_ly
            , CAST(0 as DECIMAL(20,2)) AS sale_strff_$_ly
            , CAST(0 as DECIMAL(12,0)) AS sale_strff_units_ly
            --LY Returns
            , CAST(0 as DECIMAL(20,2)) AS rtrn_$_ly
            , CAST(0 as DECIMAL(12,0)) AS rtrn_units_ly
            , CAST(0 as DECIMAL(20,2)) AS rtrn_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS rtrn_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS rtrn_pro_$_ly
            , CAST(0 as DECIMAL(12,0)) AS rtrn_pro_units_ly
            , CAST(0 as DECIMAL(20,2)) AS rtrn_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS rtrn_clr_units_ly
            --LY Demand
            , CAST(0 as DECIMAL(20,2)) AS demand_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_units_ly
            , CAST(0 as DECIMAL(20,2)) AS demand_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS demand_pro_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_pro_units_ly
            , CAST(0 as DECIMAL(20,2)) AS demand_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_clr_units_ly
            , CAST(0 as DECIMAL(20,2)) AS demand_ds_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_ds_units_ly
            , CAST(0 as DECIMAL(20,2)) AS demand_fls_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_fls_units_ly
            , CAST(0 as DECIMAL(20,2)) AS demand_fc_$_ly
            , CAST(0 as DECIMAL(12,0)) AS demand_fc_units_ly
            --LY Ship
            , CAST(0 as DECIMAL(20,2)) AS shipped_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_units_ly
            , CAST(0 as DECIMAL(20,2)) AS shipped_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS shipped_pro_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_pro_units_ly
            , CAST(0 as DECIMAL(20,2)) AS shipped_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_clr_units_ly
            , CAST(0 as DECIMAL(20,2)) AS shipped_ds_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_ds_units_ly
            , CAST(0 as DECIMAL(20,2)) AS shipped_fls_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_fls_units_ly
            , CAST(0 as DECIMAL(20,2)) AS shipped_fc_$_ly
            , CAST(0 as DECIMAL(12,0)) AS shipped_fc_units_ly
            --LY Receipts
            , CAST(0 as DECIMAL(20,2)) AS receipt_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_po_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_po_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_po_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_po_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_po_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_po_clr_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_rsk_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_rsk_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_rsk_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_rsk_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_rsk_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_rsk_clr_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_dsh_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_dsh_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_dsh_reg_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_dsh_reg_units_ly
            , CAST(0 as DECIMAL(20,2)) AS receipt_dsh_clr_$_ly
            , CAST(0 as DECIMAL(12,0)) AS receipt_dsh_clr_units_ly
            --LY EOH
            , SUM(COALESCE(inventory_$_ly,0)) AS inventory_$_ly
            , SUM(COALESCE(inventory_units_ly,0)) AS inventory_units_ly
            , SUM(COALESCE(inventory_regpro_$_ly,0)) AS inventory_regpro_$_ly
            , SUM(COALESCE(inventory_regpro_units_ly,0)) AS inventory_regpro_units_ly
            , SUM(COALESCE(inventory_clr_$_ly,0)) AS inventory_clr_$_ly
            , SUM(COALESCE(inventory_clr_units_ly,0)) AS inventory_clr_units_ly

        FROM
        (
            SELECT
                fact.sku_number
                , fact.fiscal_week
                , fact.fiscal_month
                , SUM(inventory_$) AS inventory_$_ty
                , SUM(inventory_units) AS inventory_units_ty
                , SUM(inventory_regpro_$) AS inventory_regpro_$_ty
                , SUM(inventory_regpro_units) AS inventory_regpro_units_ty
                , SUM(inventory_clr_$) AS inventory_clr_$_ty
                , SUM(inventory_clr_units) AS inventory_clr_units_ty

            FROM
                inv_pstn_sku_cw fact
                INNER JOIN skus
                    ON fact.sku_number = skus.sku_number
                INNER JOIN weeks
                    ON fact.fiscal_week = weeks.fiscal_week

            WHERE
                fact.fiscal_week BETWEEN (SELECT start_fiscal_week FROM week_range) AND (SELECT end_fiscal_week FROM week_range)
                AND fact.channel_number = 120

            GROUP BY
                fact.sku_number
                , fact.fiscal_week
                , fact.fiscal_month
        ) ty
        FULL OUTER JOIN
        (
            SELECT
                fact.sku_number
                , weeks.next_yr_fiscal_week AS fiscal_week --magic
                , weeks.next_yr_fiscal_month AS fiscal_month --magic
                , SUM(inventory_$) AS inventory_$_ly
                , SUM(inventory_units) AS inventory_units_ly
                , SUM(inventory_regpro_$) AS inventory_regpro_$_ly
                , SUM(inventory_regpro_units) AS inventory_regpro_units_ly
                , SUM(inventory_clr_$) AS inventory_clr_$_ly
                , SUM(inventory_clr_units) AS inventory_clr_units_ly

            FROM
                inv_pstn_sku_cw fact
                INNER JOIN skus
                    ON fact.sku_number = skus.sku_number
                INNER JOIN weeks
                    ON fact.fiscal_week = weeks.fiscal_week

            WHERE
                --magic
                weeks.next_yr_fiscal_week BETWEEN (SELECT start_fiscal_week FROM week_range) AND (SELECT end_fiscal_week FROM week_range)
                AND fact.channel_number = 120

            GROUP BY
                fact.sku_number
                , weeks.next_yr_fiscal_week
                , weeks.next_yr_fiscal_month
        ) ly
            ON ty.sku_number = ly.sku_number
            AND ty.fiscal_week = ly.fiscal_week
            AND ty.fiscal_month = ly.fiscal_month

        GROUP BY
            COALESCE(ty.sku_number, ly.sku_number)
            , COALESCE(ty.fiscal_week, ly.fiscal_week)
            , COALESCE(ty.fiscal_month, ly.fiscal_month)
    --END INVENTORY QUERY
) sub

GROUP BY
    sku_number
    , fiscal_week
    , fiscal_month
;

DROP TABLE skus;
DROP TABLE locations;
DROP TABLE weeks;
DROP TABLE week_range;

COLLECT STATISTICS 
     COLUMN(sku_number, fiscal_week)
    ,COLUMN(fiscal_month)
ON prd_ma_digitalbi.digital_merch_sku_weekly;