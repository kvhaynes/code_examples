# Code Examples

This repository contains examples of professional code created by Kevin Haynes.
Table and column names have been altered for security and compliance with NDA.

## Contents

### Questionnaire Answer
* questionnaire.sql - This was uploaded as an answer for the GitLab job
application questionnaire.

### Redshift

* category_traffic_daily.sql - This was designed to build a table for reporting
that measured traffic to ecommerce category browse pages.
* anni_product_demand.sql - This was used in Tableau to provide sale
event orders mapped to product hierarchy and customers' participation in the
loyalty program.
* live_on_site.sql - This was used in Tableau to track how many products were
available to customers on the website in a given month with dimensions around
whether or not the products made it through the site funnel.

### Teradata

* digital_merch_sku_weekly.sql - This was designed to build a reusable table for
analysis and reporting around a product's sales, returns, demand, receipts, and
inventory over time.
* product_sell_through.sql - This was built into a feed to a third-party vendor
so that they could optimize marketing campaigns by how likely products were to
sell out.