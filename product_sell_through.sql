/*
This query identifies all the SKUs that have inventory in a direct location
as of yesterday as well as what that inventory quantity is for each SKU.
It then pulls net sales units for the past 13 weeks + the current week to date.
If the query is run on a Sunday, it will just return the 13 weeks of data
because there is no WTD in progress.

Tables used:
    * inventory_sku_day:
        Inventory position by SKU, location, and day. Used to pull ending on hand (EOH)
    * location_lookup:
        Location hierarchy table from the merchant finacial planning (MFP) application.
        Used to filter on the Direct/N.com channel (channel_number 120)
    * last_load_lookup:
        Single record lookup for the most-recent date of data loaded into the database.
        Should show yesterday once the daily batch is complete.
    * week_lookup:
        Fiscal week dimension lookup table. Used for date math on week start date
    * time_day_lkup_vw:
        Fiscal day dimension lookup table.
    * sale_dmnd_sku_ld:
        Sales and demand data by SKU, location and day. Used to pull sales units.

Written by: Kevin Haynes
Written on: 5/30/19
*/

--CTE to get all SKUs with ending on hand (EOH) in the direct channel as of yesterday
WITH inv_skus (sku_number, eoh_units) AS
(
    SELECT
        inventory.sku_number
        , SUM(inventory.inventory_units) AS eoh_units

    FROM
        inventory_sku_day inventory
        INNER JOIN location_lookup location
            ON inventory.loc_number = location.loc_number
        INNER JOIN last_load_lookup yesterday
            ON inventory.day_number = yesterday.day_number
            AND inventory.fiscal_week = yesterday.fiscal_week

    WHERE location.channel_number = '120' --Direct/N.com channel

    GROUP BY 1

    HAVING SUM(inventory.inventory_units) <> 0
)

--CTE to get all the days that we want to look at for sales units
, days (day_number, yesterday, last_7, last_14, last_28) AS
(
    SELECT
        day_number
        , CASE WHEN day_dt = (SELECT day_dt FROM last_load_lookup) THEN 1 ELSE 0 END AS yesterday
        , CASE WHEN day_dt >= (SELECT day_dt FROM last_load_lookup) - 6 THEN 1 ELSE 0 END AS last_7
        , CASE WHEN day_dt >= (SELECT day_dt FROM last_load_lookup) - 13 THEN 1 ELSE 0 END AS last_14
        , CASE WHEN day_dt >= (SELECT day_dt FROM last_load_lookup) - 27 THEN 1 ELSE 0 END AS last_28

    FROM time_day_lkup_vw

    WHERE day_dt BETWEEN
        (
            SELECT
                CASE
                    --If the last-loaded day is a Saturday, we want the full 13 weeks
                    WHEN yesterday.day_desc = 'SATURDAY' THEN twl.wk_start_dt - (12 /*weeks*/ * 7 /*days*/)
                    --For anything other than Saturday, we want the 13 weeks and this week to date (WTD)
                    ELSE twl.wk_start_dt - (13 /*weeks*/ * 7 /*days*/)
                END AS start_date
                
            FROM
                last_load_lookup yesterday
                INNER JOIN week_lookup twl  
                    ON yesterday.fiscal_week = twl.fiscal_week
        )
        AND
        (
            SELECT day_dt
            FROM last_load_lookup
        )
)

--CTE to get all the sales units for the inventory we found at Direct
, sales (sku_number, yesterday_sales_units, last_7_days_sales_units, last_14_days_sales_units, last_28_days_sales_units, last_13_weeks_sales_units) AS
(
    SELECT
        sales.sku_number
        , SUM(CASE WHEN days.yesterday = 1 THEN sales.sales_units ELSE 0 END) AS yesterday_sales_units
        , SUM(CASE WHEN days.last_7 = 1 THEN sales.sales_units ELSE 0 END) AS last_7_days_sales_units
        , SUM(CASE WHEN days.last_14 = 1 THEN sales.sales_units ELSE 0 END) AS last_14_days_sales_units
        , SUM(CASE WHEN days.last_28 = 1 THEN sales.sales_units ELSE 0 END) AS last_28_days_sales_units
        , SUM(sales.sales_units) AS last_13_weeks_sales_units

    FROM
        sale_dmnd_sku_ld sales
        INNER JOIN inv_skus
            ON sales.sku_number = inv_skus.sku_number
        INNER JOIN days
            ON sales.day_number = days.day_number
        INNER JOIN location_lookup location
            ON sales.location = location.location

    WHERE location.channel_number = '120' --Direct/N.com channel

    GROUP BY 1
)

--Now we can pull sales units and EOH
SELECT
    inv_skus.sku_number
    , inv_skus.eoh_units
    , COALESCE(sales.yesterday_sales_units, 0) AS yesterday_sales_units
    , COALESCE(sales.last_7_days_sales_units, 0) AS last_7_days_sales_units
    , COALESCE(sales.last_14_days_sales_units, 0) AS last_14_days_sales_units
    , COALESCE(sales.last_28_days_sales_units, 0) AS last_28_days_sales_units
    , COALESCE(sales.last_13_weeks_sales_units, 0) AS last_13_weeks_sales_units
    , CASE
        WHEN (COALESCE(sales.yesterday_sales_units,0) + inv_skus.eoh_units) <> 0
            THEN COALESCE(sales.yesterday_sales_units,0) / (COALESCE(sales.yesterday_sales_units,0) + inv_skus.eoh_units) 
        ELSE 0
        END AS yesterday_sell_thru_units_pct
    , CASE
        WHEN (COALESCE(sales.last_7_days_sales_units,0) + inv_skus.eoh_units) <> 0
            THEN COALESCE(sales.last_7_days_sales_units,0) / (COALESCE(sales.last_7_days_sales_units,0) + inv_skus.eoh_units) 
        ELSE 0
        END AS last_7_days_sell_thru_units_pct
    , CASE
        WHEN (COALESCE(sales.last_14_days_sales_units,0) + inv_skus.eoh_units) <> 0
            THEN COALESCE(sales.last_14_days_sales_units,0) / (COALESCE(sales.last_14_days_sales_units,0) + inv_skus.eoh_units) 
        ELSE 0
        END AS last_14_days_sell_thru_units_pct
    , CASE
        WHEN (COALESCE(sales.last_28_days_sales_units,0) + inv_skus.eoh_units) <> 0
            THEN COALESCE(sales.last_28_days_sales_units,0) / (COALESCE(sales.last_28_days_sales_units,0) + inv_skus.eoh_units) 
        ELSE 0
        END AS last_28_days_sell_thru_units_pct
    , CASE
        WHEN (COALESCE(sales.last_13_weeks_sales_units,0) + inv_skus.eoh_units) <> 0
            THEN COALESCE(sales.last_13_weeks_sales_units,0) / (COALESCE(sales.last_13_weeks_sales_units,0) + inv_skus.eoh_units) 
        ELSE 0
        END AS last_13_weeks_sell_thru_units_pct

FROM
    inv_skus
    LEFT JOIN sales
        ON inv_skus.sku_number = sales.sku_number