SELECT
	demand.order_dt
	, demand.cust_type
	, demand.level
	, sku.division
	, sku.subdivision
	, sku.department
	, sku.class
	, sku.supplier
	, sku.brand_name
	, SUM(demand.com_demand) AS com_demand
	, SUM(demand.com_order_units) AS com_order_units

FROM
	(
		SELECT
		    com.sku_number
		    , com.order_dt
		    , COALESCE(ord_lvl.cust_type, 'Non-Loyalty') AS cust_type
		    , COALESCE(ord_lvl.nordy_status, 'Non-Loyalty') as level
		    , SUM(com.com_demand) AS com_demand
		    , SUM(com.com_order_units) AS com_order_units
		    
		FROM
		    (
		        SELECT
		            order_number
		            , sku_number
		            , order_dt
		            , SUM(order_amt) AS com_demand
		            , SUM(order_qty) AS com_order_units
		            
		        FROM order_line_item
		        
		        WHERE
		            order_dt BETWEEN CAST('2019-07-09' AS DATE) AND LEAST(CAST('2019-08-04' AS DATE), TRUNC(CONVERT_TIMEZONE('PST8PDT', GETDATE())) - 1)
		            AND order_dt != CAST(COALESCE(item_cancel_dt, '2099-01-01') AS DATE) --same day cancels
		            AND COALESCE(primary_upc,'-1') <> '-1' -- Exclude items without a UPC
		            AND order_amt > 0 /*Exclude fragrance and beauty samples*/
		            AND COALESCE(LOWER(cancel_reason), 'not') NOT LIKE '%fraud%' --remove fraud
		            
		        GROUP BY 1,2,3
		    ) com
		    LEFT JOIN
		    (
		        SELECT DISTINCT
		            CAST(order_num AS VARCHAR(16)) AS order_number
		            , CASE
		                WHEN lvl.curr_level = '4751' OR lvl.curr_level IS NULL THEN 'Nordy Insider'
		                WHEN lvl.curr_level = '4752' THEN 'Nordy Influencer'
		                WHEN lvl.curr_level = '4753' THEN 'Nordy Ambassador'
		                ELSE lvl.curr_level
		                END as nordy_status
					, CASE
						WHEN en.cardmember_fl = 1 or lvl.curr_level = 'Nordy ICON' then 'Cardmember'
						ELSE 'Member'
						END as cust_type
		            
		        FROM
		            loyalty_dashboard_spend lds
		            LEFT JOIN rwrd_level_fct_vw lvl
		                ON lds.loyalty_id = lvl.loyalty_id
					LEFT JOIN loyalty_dashboard_enrollment en
						ON lds.loyalty_id = en.loyalty_id
		                
		        WHERE lds.bus_dt BETWEEN CAST('2019-07-09' AS DATE) AND LEAST(CAST('2019-08-04' AS DATE), TRUNC(CONVERT_TIMEZONE('PST8PDT', GETDATE())) - 1)
		    ) ord_lvl
		        ON com.order_number = ord_lvl.order_number
		        
		GROUP BY 1,2,3,4
	) demand
	INNER JOIN
	(
		SELECT DISTINCT
			sku.sku_number
			, sku.division_number || ', ' || sku.division_desc AS division
			, sku.subdivision_number || ', ' || sku.subdivision_desc AS subdivision
			, sku.department_number || ', ' || sku.department_desc AS department
			, sku.class_number || ', ' || sku.class_desc AS class
			, sku.supplier
			, sup_brand.brand_name
			
		FROM
			sku_lookup sku
			LEFT JOIN
			(
				SELECT sku_number
				FROM sku_lookup
				WHERE
					dm_recd_curr_flag = 'Y'
					AND prmy_upc_ind = 'Y'
				GROUP BY 1
				HAVING COUNT(*) > 1
			) dedupe
				ON sku.sku_number = dedupe.sku_number
			LEFT JOIN supplier_lookup sup
				ON sku.supplier = sup.supp_number
			LEFT JOIN supplier_brand_lookup sup_brand
				ON sku.supplier = sup_brand.supp_number
		
		WHERE
			sku.dm_recd_curr_flag = 'Y'
			AND sku.prmy_upc_ind = 'Y'
			AND dedupe.sku_number IS NULL
	) sku
		ON demand.sku_number = sku.sku_number
		
GROUP BY 1,2,3,4,5,6,7,8,9