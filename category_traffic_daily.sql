DROP TABLE IF EXISTS traffcat_dates;
--[[bi_prd.category_traffic_daily]]
CREATE TEMPORARY TABLE traffcat_dates
DISTSTYLE ALL
AS
(
    --Auto
    SELECT
        dateadd(DAY, -15, trunc(convert_timezone('US/Pacific', getdate()))) AS start_dt
        , dateadd(DAY, -1, trunc(convert_timezone('US/Pacific', getdate()))) AS end_dt
    /*
    --Manual
    SELECT
        TO_DATE('2017-10-01', 'YYYY-MM-DD') AS start_dt
        , TO_DATE('2017-12-30', 'YYYY-MM-DD') AS end_dt
    */
);

--Removed

/*
------------------------------
Get product views and instock at the cookie/day/platform/category level
------------------------------
*/
DROP TABLE IF EXISTS traffcat_prodvw_base;

CREATE TEMPORARY TABLE traffcat_prodvw_base
DISTSTYLE KEY
DISTKEY(cookie)
SORTKEY(activity_date)
AS
(
    SELECT
        activity_date
        , cookie
        , page_category
        , platform
        , SUM(product_views) AS product_views
        , SUM(rp_instock_views) + SUM(nrp_instock_views) AS instock_views

    FROM
    (
        --Aggregate to style group so we can join our instock data in
        SELECT
            pvs.activity_date
            , pvs.cookie
            , pvs.style_group_number
            , pvs.page_category
            , pvs.platform
            , COUNT(pvs.session) AS product_views
            , COUNT(pvs.session) * AVG(i.rp_stock_share) AS rp_instock_views
            , COUNT(pvs.session) * AVG(i.nrp_stock_share) AS nrp_instock_views

        FROM
            (
                    SELECT
                        derived_date AS activity_date
                        , style_number AS product_id
                        , split_part(style_number, '_', 1) AS style_group_number
                        , cookie
                        , session
                        , platform
                        , REPLACE(REPLACE(product_category, '\\~', '~'), '?`', '~') AS page_category

                    FROM product_views

                    WHERE
                        derived_date BETWEEN (SELECT start_dt FROM traffcat_dates) AND (SELECT end_dt FROM traffcat_dates)
                        AND etl_tstamp >= (SELECT CAST(start_dt AS TIMESTAMP) FROM traffcat_dates)
                        AND
                        (
                            --Removed
                        )
            ) pvs
            LEFT JOIN traffcat_instock_staging i
                ON pvs.activity_date = i.activity_date
                AND pvs.style_group_number = i.style_group_number

        GROUP BY 1,2,3,4,5
    ) sub

    GROUP BY 1,2,3,4
);

DROP TABLE IF EXISTS traffcat_instock_staging;

/*
------------------------------
Get add-to-bag at the cookie/day/platform/category level
------------------------------
*/
DROP TABLE IF EXISTS traffcat_add_base;

CREATE TEMPORARY TABLE traffcat_add_base
DISTSTYLE KEY
DISTKEY(cookie)
SORTKEY(activity_date)
AS
(
    SELECT
        derived_date AS activity_date
        , cookie
        , REPLACE(REPLACE(category, '\\~', '~'), '?`', '~') AS page_category
        , platform
        , SUM(quantity) AS cart_adds

    FROM cart

    WHERE
        derived_date BETWEEN (SELECT start_dt FROM traffcat_dates) AND (SELECT end_dt FROM traffcat_dates)
        AND etl_tstamp >= (SELECT CAST(start_dt AS TIMESTAMP) FROM traffcat_dates)
        AND event_name = 'add_to_cart'
        AND
        (
            --Removed
        )

    GROUP BY 1,2,3,4
);

/*
------------------------------
Get demand at the cookie/day/platform/category/order level
------------------------------
*/
DROP TABLE IF EXISTS traffcat_demand_base;

CREATE TEMPORARY TABLE traffcat_demand_base
DISTSTYLE KEY
DISTKEY(cookie)
SORTKEY(activity_date)
AS
(
    SELECT
        derived_date AS activity_date
        , cookie
        , REPLACE(REPLACE(category, '\\~', '~'), '?`', '~') AS page_category
        , platform
        , COUNT(DISTINCT orderid) AS orders
        , SUM(price) AS demand
        , SUM(quantity) AS order_units

    FROM order_items

    WHERE
        derived_date BETWEEN (SELECT start_dt FROM traffcat_dates) AND (SELECT end_dt FROM traffcat_dates)
        AND etl_tstamp >= (SELECT CAST(start_dt AS TIMESTAMP) FROM traffcat_dates)
        AND
        (
            --Removed
        )

    GROUP BY 1,2,3,4
);

/*
------------------------------
Get the cookie/day combos that define a bounce
------------------------------
*/
DROP TABLE IF EXISTS traffcat_bouncers;

CREATE TEMPORARY TABLE traffcat_bouncers
DISTSTYLE KEY
DISTKEY (cookie)
SORTKEY(derived_date)
AS
(
    SELECT
        cookie
        , derived_date
        , platform
        , COUNT(session) AS page_views

    FROM page_views

    WHERE
        derived_date BETWEEN (SELECT start_dt FROM traffcat_dates) AND (SELECT end_dt FROM traffcat_dates)
        AND etl_tstamp >= (SELECT CAST(start_dt AS TIMESTAMP) FROM traffcat_dates)

    GROUP BY 1,2,3

    HAVING COUNT(session) = 1
);

/*
------------------------------
Get the URL path through which the cookie entered
------------------------------
*/
DROP TABLE IF EXISTS traffcat_entry;

CREATE TEMPORARY TABLE traffcat_entry
DISTSTYLE KEY
DISTKEY(cookie)
SORTKEY(derived_date)
AS
(
    SELECT
        entry_time.derived_date
        , entry_time.cookie
        , entry_time.platform
        , entry_url.first_page_urlpath
        , COUNT(entry_time.*) AS entry_page_views

    FROM
        --Get the first timestamp for the cookie
        (
            SELECT
                cookie
                , derived_date
                , platform
                , MIN(first_derived_tstamp) AS min_derived_tstamp

            FROM sessions

            WHERE
                derived_date BETWEEN (SELECT start_dt FROM traffcat_dates) AND (SELECT end_dt FROM traffcat_dates)
                AND etl_tstamp >= (SELECT CAST(start_dt AS TIMESTAMP) FROM traffcat_dates)

            GROUP BY 1,2,3
        ) entry_time
        JOIN
        --Get the page url path and it's timestamp to join in and grab the first URL
        --that the cookie landed on
        (
            SELECT
                derived_date
                , cookie
                , platform
                , LOWER(first_page_urlpath) AS first_page_urlpath
                , MIN(first_derived_tstamp) AS min_derived_tstamp

            FROM sessions

            WHERE
                derived_date BETWEEN (SELECT start_dt FROM traffcat_dates) AND (SELECT end_dt FROM traffcat_dates)
                AND etl_tstamp >= (SELECT CAST(start_dt AS TIMESTAMP) FROM traffcat_dates)

            GROUP BY 1,2,3,4
        ) entry_url
            ON entry_time.cookie = entry_url.cookie
            AND entry_time.min_derived_tstamp = entry_url.min_derived_tstamp
            AND entry_time.platform = entry_url.platform
    
    --We only care about people who landed on a category page
    WHERE entry_url.first_page_urlpath LIKE '/c/%'

    GROUP BY 1,2,3,4
);

/*
------------------------------
Get page views by date/cookie/URL/platform/category for the categories
we care about - browse and brands
------------------------------
*/
DROP TABLE IF EXISTS traffcat_pgvw_nav;

CREATE TEMPORARY TABLE traffcat_pgvw_nav
DISTSTYLE KEY
DISTKEY(cookie)
SORTKEY(derived_date)
AS
(
    SELECT
        derived_date
        , cookie
        , CASE WHEN LOWER(page_urlpath) LIKE '/s/%' THEN 'product page' ELSE LOWER(page_urlpath) END AS page_urlpath
        , platform
        , REPLACE(REPLACE(page_category, '\\~', '~'), '?`', '~') AS page_category
        , COUNT(session) AS page_views

    FROM page_views
    
    WHERE
        derived_date BETWEEN (SELECT start_dt FROM traffcat_dates) AND (SELECT end_dt FROM traffcat_dates)
        AND etl_tstamp >= (SELECT CAST(start_dt AS TIMESTAMP) FROM traffcat_dates)
        AND
        (
            --Removed
        )

    GROUP BY 1,2,3,4,5
);

/*
------------------------------
This table is used to make sure we only assign one URL to a category per day.
The URL we choose is the one that has the most page views for the cat/day combo
------------------------------
*/
DROP TABLE IF EXISTS traffcat_max_url;

CREATE TEMPORARY TABLE traffcat_max_url
DISTSTYLE KEY
DISTKEY(page_category)
SORTKEY(derived_date)
AS
(
    SELECT DISTINCT
        derived_date
        , page_category
        --For the given date and category, we want the URL that has the most page views
        , FIRST_VALUE(page_urlpath IGNORE NULLS) OVER (
            PARTITION BY derived_date, page_category
            ORDER BY page_views DESC
            ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
            ) AS page_urlpath

    FROM
    (
        SELECT
            derived_date
            , page_category
            , page_urlpath
            , SUM(page_views) AS page_views
            
        FROM traffcat_pgvw_nav
        
        GROUP BY 1,2,3
    ) sub
);

/*
------------------------------
Now we combine page views with the entry and bounce tables and bring in our URLs
------------------------------
*/
DROP TABLE IF EXISTS traffcat_pgvw_merge;

CREATE TEMPORARY TABLE traffcat_pgvw_merge
DISTSTYLE KEY
DISTKEY(cookie)
SORTKEY(derived_date)
AS
(
    SELECT
        sub.derived_date
        , sub.cookie
        , sub.page_category
        , sub.platform
        , sub.page_urlpath
        , SUM(sub.page_views) AS page_views
        , SUM(entry.entry_page_views) AS entry_page_views
        , COUNT(bounce.cookie) AS one_page_visitors

    FROM
    --Combine page views with entry and bounce
    (
        SELECT
            pvs.derived_date
            , pvs.cookie
            , pvs.page_category
            , pvs.platform
            , url.page_urlpath AS page_urlpath
            , SUM(pvs.page_views) AS page_views

        FROM
            traffcat_pgvw_nav pvs
            LEFT JOIN traffcat_max_url url
                ON pvs.derived_date = url.derived_date
                AND pvs.page_category = url.page_category

        GROUP BY 1,2,3,4,5
    ) sub
    LEFT JOIN traffcat_entry entry
        ON sub.cookie = entry.cookie
        AND sub.derived_date = entry.derived_date
        AND sub.page_urlpath = entry.first_page_urlpath
        AND sub.platform = entry.platform
    LEFT JOIN traffcat_bouncers bounce
        ON sub.cookie = bounce.cookie
        AND sub.derived_date = bounce.derived_date
        AND sub.platform = bounce.platform

    GROUP BY 1,2,3,4,5
);

/*
------------------------------
Merge everything together at the category/cookie/day level
------------------------------
*/
DROP TABLE IF EXISTS traffcat_merge;

CREATE TEMPORARY TABLE traffcat_merge
DISTSTYLE KEY
DISTKEY(cookie)
SORTKEY(activity_date)
AS
(
    SELECT
        sub.activity_date
        , sub.cookie
        , sub.page_category
        , sub.platform
        , page.page_urlpath
        , page.page_views AS page_views
        , page.entry_page_views AS entry_page_views
        , page.one_page_visitors AS one_page_visitors
        , prdct.product_views
        , prdct.instock_views
        , prdct.cookie AS viewing_userid
        , add.cart_adds
        , add.cookie AS add_userid
        , dmnd.cookie AS buying_userid
        , dmnd.order_units
        , dmnd.demand
        , dmnd.orders

    FROM
        (
                SELECT
                    derived_date AS activity_date
                    , cookie
                    , page_category
                    , platform

                FROM traffcat_pgvw_merge

            UNION

                SELECT
                    activity_date
                    , cookie
                    , page_category
                    , platform

                FROM traffcat_prodvw_base

            UNION

                SELECT
                    activity_date
                    , cookie
                    , page_category
                    , platform

                FROM traffcat_add_base

            UNION

                SELECT
                    activity_date
                    , cookie
                    , page_category
                    , platform

                FROM traffcat_demand_base
        ) sub
        LEFT JOIN traffcat_pgvw_merge page
            ON sub.activity_date = page.derived_date
            AND sub.cookie = page.cookie
            AND sub.page_category = page.page_category
            AND sub.platform = page.platform
        LEFT JOIN traffcat_prodvw_base prdct
            ON sub.activity_date = prdct.activity_date
            AND sub.cookie = prdct.cookie
            AND sub.page_category = prdct.page_category
            AND sub.platform = prdct.platform
        LEFT JOIN traffcat_add_base add
            ON sub.activity_date = add.activity_date
            AND sub.cookie = add.cookie
            AND sub.page_category = add.page_category
            AND sub.platform = add.platform
        LEFT JOIN traffcat_demand_base dmnd
            ON sub.activity_date = dmnd.activity_date
            AND sub.cookie = dmnd.cookie
            AND sub.page_category = dmnd.page_category
            AND sub.platform = dmnd.platform
);

DELETE FROM bi_prd.category_traffic_daily
WHERE activity_date BETWEEN (SELECT start_dt FROM traffcat_dates) AND (SELECT end_dt FROM traffcat_dates)
;

INSERT INTO bi_prd.category_traffic_daily
SELECT * FROM traffcat_merge
;