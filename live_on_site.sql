SELECT
    available.fiscal_month
    , available.department_number
    , available.class_number
    , available.supp_number
    , available.style_group_number
    , available.display_color
    , available.fc_available
    , available.fls_available
    , available.ds_available
    , CASE WHEN COALESCE(sg_funnel.product_views, 0) > 0 THEN CAST(1 AS BOOLEAN) ELSE CAST(0 AS BOOLEAN) END AS sg_was_viewed
    , CASE WHEN COALESCE(sg_funnel.product_views, 0) > 1 THEN CAST(1 AS BOOLEAN) ELSE CAST(0 AS BOOLEAN) END AS sg_was_viewed_twice
    , CASE WHEN COALESCE(sg_funnel.cart_add_units, 0) > 0 THEN CAST(1 AS BOOLEAN) ELSE CAST(0 AS BOOLEAN) END AS sg_was_added
    , CASE WHEN COALESCE(sg_funnel.order_units, 0) > 0 THEN CAST(1 AS BOOLEAN) ELSE CAST(0 AS BOOLEAN) END AS sg_was_ordered
    , CAST(available.cc_was_added AS BOOLEAN) AS cc_was_added
    , CAST(available.cc_was_ordered AS BOOLEAN) AS cc_was_ordered

FROM
    (
        SELECT
            sub.fiscal_month
            , phc.department_number
            , phc.class_number
            , phc.supplier_number
            , sku.style_group_number
            , sku.display_color
            , MAX(sub.sku_fc_avail_flag) AS fc_available
            , MAX(sub.sku_fls_avail_flag) AS fls_available
            , MAX(sub.sku_ds_avail_flag) AS ds_available
            , MAX(COALESCE(sku_funnel.cc_added,0)) AS cc_was_added
            , MAX(COALESCE(sku_funnel.cc_ordered,0)) AS cc_was_ordered

        FROM
            (
                SELECT DISTINCT
                    d.activity_date
                    , d.fiscal_month
                    , d.fiscal_year || ' ' || d.fiscal_month_number || ' ' || LEFT(d.fiscal_month_abbreviation, 1) || LOWER(RIGHT(d.fiscal_month_abbreviation, 2)) AS fiscal_month
                    , los.sku_number
                    , los.sku_fc_avail_flag
                    , los.sku_fls_avail_flag
                    , los.sku_ds_avail_flag

                FROM
                    live_on_site los
                    JOIN dates d
                        ON los.sku_live_date_start <= d.day_dt
                        AND (los.sku_live_date_end IS NULL OR los.sku_live_date_end > d.day_dt)
            ) sub
            JOIN
            (
                SELECT
                    sku_number
                    , style_group_number
                    , display_color

                FROM
                (
                    SELECT
                        sku_number
                        , style_group_number
                        , display_color
                        , ROW_NUMBER() OVER (PARTITION BY sku_number ORDER BY record_current DESC, primary_upc_ind DESC, record_last_update_dt DESC, division_desc, subdivision_desc, department_desc, class_desc, sbclass_desc, supp_name) AS rownum

                    FROM sku_lookup

                    WHERE division_number NOT IN ('-1','54','90','91','92','93','94','95','96','97','98','99','120','800','900')
                ) sub

                WHERE rownum = 1
            ) sku
                ON sub.sku_number = sku.sku_number
            LEFT JOIN product_hierarchy_current phc
                ON sku.style_group_number = phc.style_group_number
            LEFT JOIN
            (
                SELECT
                    fiscal_month
                    , sku_number
                    , MAX(cc_added) AS cc_added
                    , MAX(cc_ordered) AS cc_ordered

                FROM
                (
                        SELECT DISTINCT
                            d.fiscal_month
                            , com.sku_number AS sku_number
                            , 0 AS cc_added
                            , 1 AS cc_ordered

                        FROM
                            order_line_item com
                            JOIN dates d
                                ON com.order_dt = d.day_dt

                        WHERE com.order_dt >= CAST('2018-10-07' AS DATE)

                    UNION ALL

                        SELECT DISTINCT
                            d.fiscal_month
                            , c.sku AS sku_number
                            , 1 AS cc_added
                            , 0 AS cc_ordered

                        FROM
                            cart c
                            JOIN dates d
                                ON c.derived_date = d.day_dt

                        WHERE c.derived_date >= CAST('2018-10-07' AS DATE)
                ) cc_sub

                GROUP BY 1,2
            ) sku_funnel
                ON sub.sku_number = sku_funnel.sku_number
                AND sub.fiscal_month = sku_funnel.fiscal_month

        WHERE
            --Because data was incomplete prior to mid-September, I'm using the start of fiscal October as the start of "good" live on site data
            sub.activity_date >= CAST('2018-10-07' AS DATE)
            AND sub.activity_date < CAST(CONVERT_TIMEZONE('US/Pacific', GETDATE()) AS DATE)

        GROUP BY 1,2,3,4,5,6
    ) available
    LEFT JOIN
    (
        SELECT
            d.fiscal_year || ' ' || d.fiscal_month_number || ' ' || LEFT(d.fiscal_month_abbreviation, 1) || LOWER(RIGHT(d.fiscal_month_abbreviation, 2)) AS fiscal_month
            , sgdf.style_group_number
            , SUM(sgdf.product_views) AS product_views
            , SUM(sgdf.items_added) AS cart_add_units
            , SUM(sgdf.demand) AS demand
            , SUM(sgdf.items_sold) AS order_units

        FROM
            style_group_daily_funnel sgdf
            JOIN dates d
                ON sgdf.view_date = d.day_dt

        WHERE sgdf.view_date >= CAST('2018-10-07' AS DATE)

        GROUP BY 1,2
    ) sg_funnel
        ON available.fiscal_month = sg_funnel.fiscal_month
        AND available.style_group_number = sg_funnel.style_group_number
